#!/bin/sh
set -e

g++ gloo/examples/example1.cc -lgloo -o example1

PREFIX=test1 SIZE=2 RANK=0 ./example1 &
PREFIX=test1 SIZE=2 RANK=1 ./example1

exit 0
